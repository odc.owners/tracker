/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"go.bug.st/serial"
	"log"
)

type Time struct {
	Year        byte
	Month       byte
	Day         byte
	Hour        byte
	Minute      byte
	Second      byte
	Millisecond int16
}

type Acc struct {
	A [3]int16
	T int16
}

type Gyro struct {
	W [3]int16
	T int16
}

type Angle struct {
	Angle [3]int16
	T     int16
}

type Mag struct {
	H [3]int16
	T int16
}

type DStatus struct {
	DStatus [4]int16
}

type Pressure struct {
	Pressure int32
	Altitude int32
}

type LonLat struct {
	Lon int
	Lat int
}

type GPSV struct {
	GPSHeight   int16
	GPSYaw      int16
	GPSVelocity int16
}

func ListPorts() {
	ports, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}
	if len(ports) == 0 {
		log.Fatal("No serial ports found!")
	}
	for _, port := range ports {
		fmt.Printf("Found port: %v\n", port)
	}
}

func OpenPort() serial.Port {
	mode := &serial.Mode{
		BaudRate: 9600,
		Parity:   serial.NoParity,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open("/dev/ttyUSB0", mode)
	if err != nil {
		log.Fatal(err)
	}
	port.SetDTR(true)
	port.SetRTS(false)

	return port
}

func readIntoStruct(buff []byte, out interface{}) {
	buffer := bytes.NewBuffer(buff)
	err := binary.Read(buffer, binary.LittleEndian, out)
	if err != nil {
		log.Fatal("binary.Read failed", err)
	}
}

const (
	ACCELERATION     = 0x51
	ANGULAR_VELOCITY = 0x52
	ANGLE            = 0x53
	MAGNETIC         = 0x54
	DATA_PORT_STATUS = 0x55
	PRESSURE_ALT     = 0x56
	LONG_LAT         = 0x57
	GROUND_SPEED     = 0x58
	QUATERNION       = 0x59
	GPS_ACCURACY     = 0x5A
)

func main() {
	port := OpenPort()

	for {
		buff := make([]byte, 2000)
		n, err := port.Read(buff)
		if err != nil {
			log.Fatal(err)
			break
		}

		if n == 0 {
			fmt.Println("\nEOF")
			break
		}

		for n >= 11 {
			if buff[0] != 0x55 {
				n--
				buff = buff[1:]
				continue
			}

			switch buff[1] {
			case 0x50:
				var time Time
				readIntoStruct(buff[2:10], &time)
				fmt.Printf("Time: 20%d-%d-%d %d:%d:%.3f\n",
					time.Year,
					time.Month,
					time.Day,
					time.Hour,
					time.Minute,
					float32(time.Second)+float32(time.Millisecond/1000))
			case 0x51: //
				var acc Acc
				readIntoStruct(buff[2:10], &acc)
				fmt.Printf("Acc: %.3f %.3f %.3f\n",
					float32(acc.A[0])/32768*16,
					float32(acc.A[1])/32768*16,
					float32(acc.A[2])/32768*16)
			case 0x52:
				var gyro Gyro
				readIntoStruct(buff[2:10], &gyro)
				fmt.Printf("Gyro: %.3f %.3f %.3f\r\n",
					float32(gyro.W[0])/32768*2000,
					float32(gyro.W[1])/32768*2000,
					float32(gyro.W[2])/32768*2000)
			case 0x53:
				var angle Angle
				readIntoStruct(buff[2:10], &angle)
				fmt.Printf("Angle: %.3f %.3f %.3f\r\n",
					float32(angle.Angle[0])/32768*180,
					float32(angle.Angle[1])/32768*180,
					float32(angle.Angle[2])/32768*180)
			case 0x54:
				var mag Mag
				readIntoStruct(buff[2:10], &mag)
				fmt.Printf("Mag: %d %d %d\r\n",
					mag.H[0],
					mag.H[1],
					mag.H[2])
			case 0x55:
				var dstatus DStatus
				readIntoStruct(buff[2:10], &dstatus)
				fmt.Printf("DStatus:%d %d %d %d\n",
					dstatus.DStatus[0],
					dstatus.DStatus[1],
					dstatus.DStatus[2],
					dstatus.DStatus[3])
			case 0x56:
				var press Pressure
				readIntoStruct(buff[2:10], &press)
				fmt.Printf("Pressure: %d Height: %.2f\n",
					press.Pressure,
					float32(press.Altitude)/100)
			case 0x57:
				var lonlat LonLat
				readIntoStruct(buff[2:10], &lonlat)
				fmt.Printf("Longitude: %ld Deg: %.5fm Lattitude: %ld Deg: %.5fm\n",
					lonlat.Lon/10000000,
					float64(lonlat.Lon%10000000)/1e5,
					lonlat.Lat/10000000,
					float64(lonlat.Lat%10000000)/1e5)
			case 0x58:
				var gpsv GPSV
				readIntoStruct(buff[2:10], &gpsv)
				fmt.Printf("GPSHeight: %.1fm GPSYaw: %.1fDeg GPSV: %.3fkm/h\n",
					float32(gpsv.GPSHeight)/10,
					float32(gpsv.GPSYaw)/10,
					float32(gpsv.GPSVelocity)/1000)
			}
			n -= 11
			buff = buff[11:]
		}
	}
}
