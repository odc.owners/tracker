/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	//"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/golang/protobuf/proto"
	_ "github.com/golang/protobuf/proto"
	pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"gitlab.com/skyhuborg/tracker/internal/common"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gitlab.com/skyhuborg/tracker/internal/sensor"
	"gitlab.com/skyhuborg/tracker/internal/tracker"
	"gitlab.com/skyhuborg/tracker/internal/upload"
	"gitlab.com/skyhuborg/tracker/internal/video"
	"gitlab.com/skyhuborg/trackerdb"

	"net/http"
	_ "net/http/pprof"
	"net/url"
)

type Environment struct {
	Profile         bool
	Debug           bool
	DbPath          string
	DataPath        string
	ConfigFile      string
	TrackerdAddr    string
	UploadAddr      string
	GpsAddr         string
	VideoDevice     string
	ShowWindow      bool
	DisableRecorder bool
}

type Tracker struct {
	// Id of Tracker instance
	id string
	// Human readable name of node
	node_name string
	// Command line and Environment variables
	env Environment
	// GRPC Client handle for the trackerd
	client tracker.ClientGRPC
	// GRPC Client configuration
	config tracker.ClientGRPCConfig
	// GRPC Client handle for the trackerd
	uploadClient upload.Client
	// GRPC Client configuration
	uploadClientConfig upload.ClientConfig
	// SensorMonitor that monitors all sensors
	sensorMonitor sensor.Monitor
	// Configuration settings for the tracker
	settings common.Config
	// database handle
	db trackerdb.DB
	// state object for events
	state *event.State
	/* register output channels */
	dbChannel chan *pb.SensorReport
	videoChannel chan video.VideoRecording
}

var env Environment
var version string

func InitializeStore() {
	videoDir := fmt.Sprintf("%s/video", env.DataPath)
	thumbnailDir := fmt.Sprintf("%s/thumbnail", env.DataPath)
	etcDir := fmt.Sprintf("%s/etc", env.DataPath)

	if _, err := os.Stat(etcDir); os.IsNotExist(err) {
		os.MkdirAll(etcDir, 0755)
	}
	if _, err := os.Stat(videoDir); os.IsNotExist(err) {
		os.MkdirAll(videoDir, 0755)
	}
	if _, err := os.Stat(thumbnailDir); os.IsNotExist(err) {
		os.MkdirAll(thumbnailDir, 0755)
	}
}

func DbOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
	var (
		rec    trackerdb.Sensor
		err    error
		report *pb.SensorReport
		data   []byte
	)

	log.Printf("Started DB Video Logging thread\n")

	for {
		report = <-ch

		data, err = proto.Marshal(report)

		if err != nil {
			log.Printf("Failed Marshaling SensorReport: %s\n", err)
			continue
		}
		rec.EventId = report.EventId
		rec.Data = data
		tracker.db.AddSensorData(&rec)
	}
}

func DbOutputVideoThread(tracker *Tracker, ch chan video.VideoRecording) {
	var (
		videoRecording video.VideoRecording
	)

	log.Printf("Started DB Logging thread\n")

	for {
		videoRecording = <-ch
		tracker.db.AddVideoEvent(videoRecording.EventId, videoRecording.Uri, videoRecording.Thumbnail)
	}
}

func VideoUploadThread(t *Tracker) {
	var (
		videoEvent trackerdb.VideoEvent
		err        error
	)

	for {
		videoEvent, err = t.db.GetVideoEventNotUploaded()

		if err != nil {
			goto timeout
		}

		videoEvent.IsPending = true
		t.db.Save(videoEvent)

		err = t.uploadClient.Upload(videoEvent.EventId, videoEvent.Uri, videoEvent.Thumbnail)

		if err != nil {
			log.Printf("Error: Upload failed  %s\n", err)
			videoEvent.IsPending = false
			t.db.Save(videoEvent)
			goto timeout
		}
		videoEvent.IsUploaded = true
		t.db.Save(videoEvent)

		continue
	timeout:
		time.Sleep(5 * time.Second)
	}
}

func TrackerOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
	//client := tracker.client
	/*
		client := pb.NewTrackerdClient(conn)
		for {
			report := <-ch
			_, err := client.AddSensor(context.Background(), report)

			if err != nil {
				log.Printf("Error: %s\n", err)
			}
		}
	*/
}

func parseArgs() {
	paramDbPath := "db-path"
	paramDataPath := "data-path"
	paramConfigFile := "config-file"
	paramTrackerdAddr := "trackerd-server-addr"
	paramUploadAddr := "upload-server-addr"
	paramGpsAddr := "gpsd-addr"
	paramVideoDeviceOverride := "video-device"
	paramShowWindow := "show-window"
	paramDebug := "debug"
	paramProfile := "profile"
	paramDisableRecorder := "disable-recorder"

	envDbPath := os.Getenv(paramDbPath)
	envDataPath := os.Getenv(paramDataPath)
	envConfigFile := os.Getenv(paramConfigFile)
	envTrackerdAddr := os.Getenv(paramTrackerdAddr)
	envUploadAddr := os.Getenv(paramUploadAddr)
	envGpsAddr := os.Getenv(paramGpsAddr)
	envVideoDeviceOverride := os.Getenv(paramVideoDeviceOverride)
	envShowWindow := os.Getenv(paramShowWindow)
	envDebug := os.Getenv(paramDebug)
	envProfile := os.Getenv(paramProfile)
	envDisableRecorder := os.Getenv(paramDisableRecorder)

	flag.StringVar(&env.DbPath, paramDbPath, "/skyhub/db/tracker.db", "path to database file")
	flag.StringVar(&env.DataPath, paramDataPath, "/skyhub/data", "path to data directory")
	flag.StringVar(&env.ConfigFile, paramConfigFile, "/skyhub/etc/tracker.yml", "path to config file")
	flag.StringVar(&env.TrackerdAddr, paramTrackerdAddr, "localhost:8088", "Trackerd server address")
	flag.StringVar(&env.UploadAddr, paramUploadAddr, "localhost:8090", "Upload server address")
	flag.StringVar(&env.GpsAddr, paramGpsAddr, "localhost:2947", "GPSD server address")
	flag.StringVar(&env.VideoDevice, paramVideoDeviceOverride, "", "Override the video capture device name")
	flag.BoolVar(&env.ShowWindow, paramShowWindow, false, "Display the video window")
	flag.BoolVar(&env.Debug, paramDebug, false, "Enable debugging")
	flag.BoolVar(&env.Profile, paramProfile, false, "Enable profiling")
	flag.BoolVar(&env.DisableRecorder, paramDisableRecorder, false, "Disable recorder")
	flag.Parse()

	if len(envDbPath) > 0 {
		env.DbPath = envDbPath
	}

	if len(envDataPath) > 0 {
		env.DataPath = envDataPath
	}

	if len(envConfigFile) > 0 {
		env.ConfigFile = envConfigFile
	}

	if len(envTrackerdAddr) > 0 {
		env.TrackerdAddr = envTrackerdAddr
	}

	if len(envUploadAddr) > 0 {
		env.UploadAddr = envUploadAddr
	}

	if len(envGpsAddr) > 0 {
		env.GpsAddr = envGpsAddr
	}

	if len(envShowWindow) > 0 {
		tempShowWindow, err := strconv.ParseBool(envShowWindow)
		if err == nil {
			env.ShowWindow = bool(tempShowWindow)
		}
	}

	if len(envVideoDeviceOverride) > 0 {
		env.VideoDevice = envVideoDeviceOverride
	}

	if len(envDebug) > 0 {
		tempDebug, err2 := strconv.ParseBool(envDebug)
		if err2 == nil {
			env.Debug = bool(tempDebug)
		}
	}

	if len(envProfile) > 0 {
		tempProfile, err2 := strconv.ParseBool(envProfile)
		if err2 == nil {
			env.Profile = bool(tempProfile)
		}
	}

	if len(envDisableRecorder) > 0 {
		tempDisableRecorder, err2 := strconv.ParseBool(envDisableRecorder)
		if err2 == nil {
			env.DisableRecorder = bool(tempDisableRecorder)
		}
	}
}

func NewTracker(env Environment) (t Tracker, err error) {
	var (
		// handle for the gpsd sensor
		gpsdSensor sensor.GPSDSensor
		// handle for the witmotion hw901/jy901 sensor
		jy901Sensor sensor.JY901Sensor
	)

	t.env = env

	err = t.db.Open(env.DbPath)

	if err != nil {
		return
	}

	// Initialize the subscriber map
	t.state = event.NewState(&t.db)

	// Load the local configuration for the tracker
	err = t.settings.Open(env.ConfigFile)

	if err != nil {
		log.Printf("Error: %s\n", err)
		return
	}

	defer t.settings.Close()

	t.id = t.settings.GetUuid()

	if err != nil {
		log.Fatalf("Error: Failed getting Trackerd Id")
		return
	}

	t.node_name = t.settings.GetNodeName()

	if err != nil {
		log.Fatalf("Error: failed getting NodeName")
		return
	}

	// configure and connect to trackerd
	/*
		clientConfig := tracker.ClientGRPCConfig{
			Address: env.TrackerdAddr}
		t.client, err = tracker.NewClientGRPC(clientConfig)

		if err != nil {
			log.Fatalf("Error: failed connecting to trackerd: %s\n", err)
			return
		}

		t.uploadClientConfig = upload.ClientConfig{
			ServerAddr: env.UploadAddr,
			ChunkSize:  4096,
		}

		t.uploadClient = upload.NewClient(&t.uploadClientConfig)

		err = t.uploadClient.Connect()

		if err != nil {
			log.Printf("Start() failed: %s\n", err)
			return
		}*/

	// create new SensorMonitor attached to the state
	t.sensorMonitor, err = sensor.NewMonitor(t.state)

	if err != nil {
		log.Fatalf("Error: failed creating sensor.Monitor: %s", err)
		return
	}

	// poll sensors every second, this will be changed to something different
	t.sensorMonitor.SetPollInterval(time.Second * 1)

	/* register output channels */
	t.dbChannel = make(chan *pb.SensorReport, 10)
	//trackerdChannel := make(chan *pb.SensorReport, 10)
	t.videoChannel = make(chan video.VideoRecording, 10)

	t.sensorMonitor.RegisterChannel(t.dbChannel)
	//t.sensorMonitor.RegisterChannel(trackerdChannel)

	//go TrackerOutputThread(&t, trackerdChannel)
	go DbOutputThread(&t, t.dbChannel)
	go DbOutputVideoThread(&t, t.videoChannel)

	/* register sensors */
	gpsdSensor.Addr = env.GpsAddr
	t.sensorMonitor.Register(&gpsdSensor)
	t.sensorMonitor.Register(&jy901Sensor)

	log.Printf("SensorMonitor:  Started sensor polling")

	go t.sensorMonitor.Poll()
	go t.Start()

	if len(t.settings.GetCameras()) > 0 {
		for _, c := range t.settings.GetCameras() {
			var (
				v         video.Video
				cameraUri *url.URL
			)

			if c.Enabled == false {
				continue
			}

			if env.DisableRecorder {
				v.DisableRecord = true
			}

			v.Init()
			v.SetInputPipeline(t.settings.GetInputPipeline())
			v.SetOutputPipeline(t.settings.GetOutputPipeline())
			v.RegisterVideoChannel(t.videoChannel)

			cameraUri, parseError := url.Parse(c.Uri)

			if parseError != nil {
				log.Printf("Invalid camera Uri: %s\n", c.Uri)
			}
			cameraUri.User = url.UserPassword(c.Username, c.Password)

			v.SetState(t.state)
			v.SetDataPath(env.DataPath)

			if len(env.VideoDevice) > 0 {
				err = v.Open(env.VideoDevice)
			} else {
				err = v.Open(cameraUri.String())
			}

			if err != nil {
				log.Printf("Failed opening device: %s\n", err)
				return
			}

			motionDetector := video.NewMotionDetector()
			motionDetector.EnableWindow(env.ShowWindow)
			motionDetector.SetMinimumArea(2000)
			motionDetector.EnableBoxing(true)
			motionDetector.SetState(t.state)

			dnnDetector := video.NewDnn()
			dnnDetector.EnableWindow(env.ShowWindow)
			dnnDetector.SetMinimumArea(2000)
			dnnDetector.EnableBoxing(true)
			dnnDetector.SetState(t.state)

			v.RegisterVideoOutput(dnnDetector)

			go v.Start()
		}
	} else {
		log.Println("No cameras detected.  Operating in Sensor only mode.")
	}

	return
}

func (t *Tracker) Stop() {

	t.client.Close()
	t.db.Close()
}

func (t *Tracker) Start() (bSuccess bool) {
	//go VideoUploadThread(t)

	log.Printf("Node=%s UUID=%s is Online\n", t.node_name, t.id)

	return
}

func main() {
	var (
		err     error
		tracker Tracker
		g_Shutdown bool
	)

	// load all command line args and env variables
	parseArgs()

	if env.Profile {
		go func() {
			log.Println(http.ListenAndServe(":6060", nil))
		}()
		log.Println("Profiling mode enabled.")
	}

	// creates the directory structure for the data path
	InitializeStore()

	// create our tracker instance and expose the Environment to it
	tracker, err = NewTracker(env)

	if err != nil {
		log.Printf("Tracker initialization failed: %s\n", err)
		return
	}

	for {
		time.Sleep(30 * time.Second)

		if g_Shutdown == true {
			break
		}
	}
	tracker.Stop()
}
