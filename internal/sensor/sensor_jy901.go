/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"bytes"
	"encoding/binary"
	"github.com/orcaman/concurrent-map"
	"go.bug.st/serial"
	"log"
)

type JY901Sensor struct {
	data         cmap.ConcurrentMap
	port         serial.Port
	pollInterval int
}

func (s *JY901Sensor) name() string {
	return "JY901"
}

func (s *JY901Sensor) openPort() (port serial.Port, err error) {
	var (
		mode *serial.Mode
	)

	mode = &serial.Mode{
		BaudRate: 9600,
		Parity:   serial.NoParity,
		StopBits: serial.OneStopBit,
	}

	port, err = serial.Open("/dev/hwt901b", mode)

	if err != nil {
		return
	}

	port.SetDTR(true)
	port.SetRTS(false)

	return
}

func readIntoStruct(buff []byte, out interface{}) {
	buffer := bytes.NewBuffer(buff)
	err := binary.Read(buffer, binary.LittleEndian, out)
	if err != nil {
		log.Fatal("binary.Read failed", err)
	}
}

func (s *JY901Sensor) read() map[string]interface{} {
	return s.data.Items()
}

func (s *JY901Sensor) init() bool {
	var (
		err error
	)

	s.data = cmap.New()
	s.port, err = s.openPort()

	if err != nil {
		log.Printf("Failed opening %s: %s\n", s.name(), err)
		return false
	}

	go s.poll()

	return true
}

func (s *JY901Sensor) close() bool {
	return true
}

func (s *JY901Sensor) poll() {
	for {
		buff := make([]byte, 2000)
		n, err := s.port.Read(buff)
		if err != nil {
			log.Fatal(err)
			break
		}

		if n == 0 {
			log.Println("\nEOF")
			break
		}

		for n >= 11 {
			if buff[0] != 0x55 {
				n--
				buff = buff[1:]
				continue
			}

			switch buff[1] {
			case 0x50:
				var time Time
				readIntoStruct(buff[2:10], &time)
				s.data.Set(TimeType, time)
			case 0x51:
				var acc Acceleration
				readIntoStruct(buff[2:10], &acc)
				s.data.Set(AccelerationType, acc)
			case 0x52:
				var gyro Gyroscope
				readIntoStruct(buff[2:10], &gyro)
				s.data.Set(GyroscopeType, gyro)
			case 0x53:
				var angle Angle
				readIntoStruct(buff[2:10], &angle)
				s.data.Set(AngleType, angle)
			case 0x54:
				var mag Magnetometer
				readIntoStruct(buff[2:10], &mag)
				s.data.Set(MagnetometerType, mag)
			/*
				case 0x55:
					var dstatus DStatus
					readIntoStruct(buff[2:10], &dstatus)
					log.Printf("DStatus:%d %d %d %d\n",
						dstatus.DStatus[0],
						dstatus.DStatus[1],
						dstatus.DStatus[2],
						dstatus.DStatus[3]);
			*/
			case 0x56:
				var press AirPressure
				readIntoStruct(buff[2:10], &press)
				s.data.Set(AirPressureType, press)
			case 0x57:
				var lonlat LonLat
				readIntoStruct(buff[2:10], &lonlat)
				s.data.Set(LonLatType, lonlat)
			case 0x58:
				var gpsv GPSV
				readIntoStruct(buff[2:10], &gpsv)
				s.data.Set(GpsVType, gpsv)
			}
			n -= 11
			buff = buff[11:]
		}
	}
}
