/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"log"
	"time"
)

type Sensor interface {
	read() map[string]interface{}
	init() bool
	close() bool
	name() string
}

type Monitor struct {
	sensors             []Sensor
	pollInterval        time.Duration
	channels            []chan *pb.SensorReport
	state               *event.State
	event               *event.Event
	rapidLoggingEnabled bool
}

func NewMonitor(state *event.State) (m Monitor, err error) {
	m.state = state
	m.rapidLoggingEnabled = false
	m.state.Ref()
	return
}

func (m *Monitor) SetPollInterval(interval time.Duration) {
	m.pollInterval = interval
}

func (m *Monitor) Poll() {
	var (
		ev            *event.Event
		activeSensors []string
	)

	for _, s := range m.sensors {
		activeSensors = append(activeSensors, s.name())
	}

	log.Printf("Started monitoring thread for sensors: %s\n", activeSensors)

	ev = m.state.GetEvent()

	for {
		isEmpty := true

		report := &pb.SensorReport{}

		if ev.GetInProgress() {
			m.rapidLoggingEnabled = true
			m.pollInterval = 1 * time.Second
		} else {
			if m.rapidLoggingEnabled {
				m.rapidLoggingEnabled = false
				m.pollInterval = 5 * time.Second
				ev.Done()
			}

		}

		for _, sensor := range m.sensors {
			data := sensor.read()

			if len(data) == 0 {
				continue
			}

			isEmpty = false

			for key, value := range data {
				switch key {
				case GpsTPVType:
					o, ok := value.(GPS_TPVReport)

					if ok == true {
						report.GpsTPV = o.Pack()
					}
				case TimeType:
					o, ok := value.(Time)

					if ok == true {
						report.Time = o.Pack()
					}

				case AccelerationType:
					o, ok := value.(Acceleration)

					if ok == true {
						report.Acceleration = o.Pack()
					}
				case GyroscopeType:
					o, ok := value.(Gyroscope)

					if ok == true {
						report.Gyroscope = o.Pack()
					}
				case AngleType:
					o, ok := value.(Angle)

					if ok == true {
						report.Angle = o.Pack()
					}
				case MagnetometerType:
					o, ok := value.(Magnetometer)

					if ok == true {
						report.Magnetometer = o.Pack()
					}
				case AirPressureType:
					o, ok := value.(AirPressure)

					if ok == true {
						report.AirPressure = o.Pack()
					}
				case LonLatType:
					o, ok := value.(LonLat)

					if ok == true {
						report.LonLat = o.Pack()
					}
				case GpsVType:
					o, ok := value.(GPSV)

					if ok == true {
						report.Gpsv = o.Pack()
					}
				default:
				}
			}
		}

		if isEmpty == false {
			for _, ch := range m.channels {
				ch <- report
			}
		}

		time.Sleep(m.pollInterval)
	}
}

func (m *Monitor) RegisterChannel(ch chan *pb.SensorReport) {
	m.channels = append(m.channels, ch)
}

func (m *Monitor) Register(s Sensor) {
	if s == nil {
		return
	}

	b := s.init()

	if b == false {
		log.Printf("Failed registering sensor: %s\n", s.name())
		return
	}
	m.sensors = append(m.sensors, s)
}
