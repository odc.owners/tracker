package upload

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

type ServerConfig struct {
	// port the upload server will listen on
	ListenPort int
	// Size of Chunk in bytes, 4096 works best.
	ChunkSize int64
	// path where video videoFiles are uploaded
	UploadPath string
	// path where video videoFiles are stored
	videoPath string
	// path where thumbnails are stored
	thumbnailPath string
}

type Server struct {
	Listener net.Listener
	Config   *ServerConfig
	Done     chan bool
}

func NewServer(Config *ServerConfig) *Server {
	var (
		s Server
	)

	// set defaults if no Config is provided
	if Config == nil {
		s.Config = &ServerConfig{}
		return &s
	}

	s.Config = Config

	// paths for thumbnail and video videoFiles
	s.Config.videoPath = fmt.Sprintf("%s/video", s.Config.UploadPath)

	s.Config.thumbnailPath = fmt.Sprintf("%s/thumbnail", s.Config.UploadPath)

	// Create the paths if they dont exist
	os.MkdirAll(s.Config.videoPath, 0755)
	os.MkdirAll(s.Config.thumbnailPath, 0755)

	s.Done = make(chan bool)

	return &s
}

func (s *Server) Poll() {
	var (
		conn net.Conn
		err  error
	)

	for {
		conn, err = s.Listener.Accept()

		if err != nil {
			log.Fatal(err)
			break
		}

		go func(s *Server, c net.Conn) {
			for {
				var (
					eventIdBuffer []byte
					eventIdName   string
					// video file
					videoFileNameBuffer []byte
					videoFileSizeBuffer []byte
					videoFileName       string
					videoFileSize       int64
					videoFile           *os.File
					videoFileUri        string
					// thumbnail file
					thumbnailFileNameBuffer []byte
					thumbnailFileSizeBuffer []byte
					thumbnailFileName       string
					thumbnailFileSize       int64
					thumbnailFile           *os.File
					thumbnailFileUri        string
					bytesRead               int64
				)

				timeStart := time.Now()

				eventIdBuffer = make([]byte, 64)
				videoFileSizeBuffer = make([]byte, 10)
				videoFileNameBuffer = make([]byte, 64)
				thumbnailFileSizeBuffer = make([]byte, 10)
				thumbnailFileNameBuffer = make([]byte, 64)

				_, err = c.Read(eventIdBuffer)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				_, err = c.Read(videoFileSizeBuffer)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				videoFileSize, _ = strconv.ParseInt(strings.Trim(string(videoFileSizeBuffer), ":"), 10, 64)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				_, err = c.Read(videoFileNameBuffer)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				_, err = c.Read(thumbnailFileSizeBuffer)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				thumbnailFileSize, _ = strconv.ParseInt(strings.Trim(string(thumbnailFileSizeBuffer), ":"), 10, 64)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				_, err = c.Read(thumbnailFileNameBuffer)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				eventIdName = strings.Trim(string(eventIdBuffer), ":")

				videoFileName = strings.Trim(string(videoFileNameBuffer), ":")
				videoFileUri = fmt.Sprintf("%s/%s", s.Config.videoPath, videoFileName)

				thumbnailFileName = strings.Trim(string(thumbnailFileNameBuffer), ":")
				thumbnailFileUri = fmt.Sprintf("%s/%s", s.Config.thumbnailPath, thumbnailFileName)

				videoFile, err = os.Create(videoFileUri)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				defer videoFile.Close()

				for {
					if (videoFileSize - bytesRead) <= s.Config.ChunkSize {
						io.CopyN(videoFile, c, (videoFileSize - bytesRead))
						_, err = c.Read(make([]byte, (bytesRead+s.Config.ChunkSize)-videoFileSize))

						if err != nil {
							log.Printf("Transfer failed. Removing %s\n", videoFileUri)
							videoFile.Close()
							os.Remove(videoFileUri)
							return
						}
						break
					}
					io.CopyN(videoFile, c, s.Config.ChunkSize)
					bytesRead += s.Config.ChunkSize
				}

				thumbnailFile, err = os.Create(thumbnailFileUri)

				if err != nil {
					log.Println(err)
					c.Close()
					return
				}

				defer thumbnailFile.Close()

				// reset bytes read for the thumbnail
				bytesRead = 0

				for {
					if (thumbnailFileSize - bytesRead) <= s.Config.ChunkSize {
						io.CopyN(thumbnailFile, c, (thumbnailFileSize - bytesRead))
						_, err = c.Read(make([]byte, (bytesRead+s.Config.ChunkSize)-thumbnailFileSize))

						if err != nil {
							log.Printf("Transfer failed. Removing %s\n", thumbnailFileUri)
							thumbnailFile.Close()
							os.Remove(thumbnailFileUri)
							return
						}
						break
					}
					io.CopyN(thumbnailFile, c, s.Config.ChunkSize)
					bytesRead += s.Config.ChunkSize
				}

				timeEnd := time.Now()
				duration := timeEnd.Sub(timeStart).Seconds()
				fmt.Printf("Receiving video EventId=%s video=%d bytes thumbnail=%d bytes from %s in %f seconds\n", eventIdName, videoFileSize, thumbnailFileSize, c.RemoteAddr().String(), duration)
			}
		}(s, conn)

		time.Sleep(1 * time.Second)
	}
	s.Done <- true
}

func (s *Server) Start() (err error) {
	var (
		port string
	)

	port = fmt.Sprintf(":%d", s.Config.ListenPort)

	s.Listener, err = net.Listen("tcp", port)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer s.Listener.Close()

	go s.Poll()

	<-s.Done

	return
}

func (s *Server) Close() {
}
