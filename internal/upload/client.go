package upload

import (
	"io"
	"log"
	"net"
	"os"
	"strconv"
)

type ClientConfig struct {
	ServerAddr string
	ChunkSize  int
}

type Client struct {
	Conn   net.Conn
	config *ClientConfig
}

func NewClient(config *ClientConfig) Client {
	var (
		c Client
	)

	// set defaults if no config is provided
	if config == nil {
		c.config = &ClientConfig{}
		return c
	}

	c.config = config

	return c
}

func fillString(retunString string, toLength int) string {
	for {
		strLength := len(retunString)
		if strLength < toLength {
			retunString = retunString + ":"
			continue
		}
		break
	}
	return retunString
}

func (c *Client) Connect() (err error) {
	c.Conn, err = net.Dial("tcp", c.config.ServerAddr)

	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (c *Client) Upload(eventId string, videoUri string, thumbnailUri string) (err error) {
	var (
		videoFile     *os.File
		thumbnailFile *os.File
	)

	videoFile, err = os.Open(videoUri)

	if err != nil {
		log.Printf("Open failed: %s\n", err)
		return
	}

	defer videoFile.Close()

	videoFileInfo, err := videoFile.Stat()
	if err != nil {
		log.Printf("Stat failed: %s\n", err)
		return
	}

	thumbnailFile, err = os.Open(thumbnailUri)

	if err != nil {
		log.Printf("Open failed: %s\n", err)
		return
	}

	defer thumbnailFile.Close()

	thumbnailFileInfo, err := thumbnailFile.Stat()
	if err != nil {
		log.Printf("Stat failed: %s\n", err)
		return
	}

	buffer := make([]byte, c.config.ChunkSize)

	eventIdName := fillString(eventId, 64)
	videoFileSize := fillString(strconv.FormatInt(videoFileInfo.Size(), 10), 10)
	videoFileName := fillString(videoFileInfo.Name(), 64)
	thumbnailFileSize := fillString(strconv.FormatInt(thumbnailFileInfo.Size(), 10), 10)
	thumbnailFileName := fillString(thumbnailFileInfo.Name(), 64)

	log.Printf("Upload Video for Event=%s to server=%s\n", eventId, c.config.ServerAddr)

	c.Conn.Write([]byte(eventIdName))
	c.Conn.Write([]byte(videoFileSize))
	c.Conn.Write([]byte(videoFileName))
	c.Conn.Write([]byte(thumbnailFileSize))
	c.Conn.Write([]byte(thumbnailFileName))

	for {
		_, err = videoFile.Read(buffer)
		if err == io.EOF {
			err = nil
			break
		}
		c.Conn.Write(buffer)
	}

	for {
		_, err = thumbnailFile.Read(buffer)
		if err == io.EOF {
			err = nil
			break
		}
		c.Conn.Write(buffer)
	}

	log.Printf("Transfer %s finished", videoFileInfo.Name())
	return
}

func (c *Client) Close() {
	c.Conn.Close()
	return
}
