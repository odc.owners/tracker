/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

package event

import (
	"github.com/google/uuid"
	"log"
	"sync"
	"time"
)

type EventType int

const (
	VideoMotion EventType = iota
	AudioNoise  EventType = iota
	All                   = iota
)

func (t EventType) String() string {
	names := [...]string{
		"VideoMotion",
		"AudioNoise",
		"All"}

	if t < VideoMotion || t > All {
		return "Unknown"
	}

	return names[t]
}

type Event struct {
	id         string    // Id of the event, randomly generated UUID hash
	inProgress bool      // true if the event is in progress
	isComplete bool      // true if the event is in progress
	startTime  time.Time // time the event started
	endTime    time.Time // time the event ended
	duration   int64     // how long the event lasted in ms
	srcType    EventType // The type of event, see EventType
	source     *Source   // source is where the event originated from
	wg         sync.WaitGroup
	refCount   int
}

func NewEvent() *Event {
	e := Event{}
	e.inProgress = false
	e.isComplete = true
	return &e
}

func (e *Event) SetWaitGroup(refCount int) {
	e.refCount = refCount
}

func (e *Event) Start() {
	if e.inProgress == true {
		return
	}

	e.wg.Add(e.refCount)

	e.isComplete = false

	e.startTime = time.Now()

	id, err := uuid.NewRandom()

	if err != nil {
		log.Printf("Error generating random uuid: %s\n", err)
		return
	}
	e.id = id.String()

	e.inProgress = true
}

func (e *Event) Stop() {
	if e.inProgress == false {
		return
	}

	e.inProgress = false

	e.endTime = time.Now()

	// wait for callers with references to finish
	e.wg.Wait()

	elapsed := e.endTime.Sub(e.startTime)

	e.duration = elapsed.Milliseconds()
	e.isComplete = true
}

/* setters */
func (e *Event) SetSource(source *Source) {
	e.source = source
}

/* setters */
func (e *Event) GetId() string {
	return e.id
}

func (e *Event) GetInProgress() bool {
	return e.inProgress
}

func (e *Event) GetIsComplete() bool {
	return e.isComplete
}

func (e *Event) GetStartTime() time.Time {
	return e.startTime
}

func (e *Event) GetEndTime() time.Time {
	return e.endTime
}

func (e *Event) GetDuration() int64 {
	return e.duration
}

func (e *Event) GetSourceType() EventType {
	return e.srcType
}

func (e *Event) GetSource() *Source {
	return e.source
}

// callers call this to indicate they are finished with the event
func (e *Event) Done() {
	e.wg.Done()
}
