package event

import "testing"

func TestNewEvent(t *testing.T) {

	e := NewEvent()

	if e == nil {
		t.Errorf("Creating event failed.")
	}
}

func TestStartEvent(t *testing.T) {
	e := NewEvent()

	if e == nil {
		t.Errorf("Create event failed.")
	}

	if e.GetInProgress() == true {
		t.Errorf("Event should not be in progress")
	}

	e.Start()

	if e.GetInProgress() == false {
		t.Errorf("Event was started, but is not in progress")
	}
}

func TestStopEvent(t *testing.T) {
	e := NewEvent()

	if e == nil {
		t.Errorf("Create event failed.")
	}

	if e.GetInProgress() == true {
		t.Errorf("Event should not be in progress")
	}

	e.Stop()

	if e.GetInProgress() == true {
		t.Errorf("Event was stopped, but is in progress")
	}
}

func TestComplete(t *testing.T) {
	e := NewEvent()

	if e == nil {
		t.Errorf("Create event failed.")
	}

	if e.GetInProgress() == true {
		t.Errorf("Event should not be in progress")
	}

	e.Start()

	if e.GetIsComplete() != false {
		t.Errorf("Event isComplete != false failed")
	}

	e.Stop()

	if e.GetIsComplete() != true {
		t.Errorf("Event isComplete != true failed")
	}

}
