package event

import (
	"fmt"
	"gitlab.com/skyhuborg/trackerdb"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

func DisableLog() {
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
}

func TestNewState(t *testing.T) {
	DisableLog()
	var (
		db  trackerdb.DB
		err error
	)

	err = db.Open("test.db")

	if err != nil {
		return
	}

	e := NewState(&db)

	if e == nil {
		t.Errorf("Creating event failed.")
	}

	db.Close()
}

func TestGetEvent(t *testing.T) {
	DisableLog()
	var (
		db  trackerdb.DB
		err error
	)

	err = db.Open("test.db")

	if err != nil {
		return
	}

	s := NewState(&db)

	if s == nil {
		t.Errorf("Creating event failed.")
	}

	e := s.GetEvent()

	if e == nil {
		t.Errorf("State contains invalid event.")
	}

	db.Close()
}

func TestGetRef(t *testing.T) {
	DisableLog()
	var (
		db  trackerdb.DB
		err error
	)

	err = db.Open("test.db")

	if err != nil {
		return
	}

	s := NewState(&db)

	if s == nil {
		t.Errorf("Creating event failed.")
	}

	if s.GetRefCount() != 0 {
		t.Errorf("Initial refCnt != 0")
	}

	s.Ref()

	if s.GetRefCount() != 1 {
		t.Errorf("Initial refCnt != 1")
	}

	s.Unref()

	if s.GetRefCount() != 0 {
		t.Errorf("Initial refCnt != 1")
	}

	db.Close()
}

func TestTrigger(t *testing.T) {
	DisableLog()

	var (
		db  trackerdb.DB
		err error
	)

	err = db.Open("test.db")

	if err != nil {
		return
	}

	s := NewState(&db)

	if s == nil {
		t.Errorf("Creating event failed.")
	}

	e := s.GetEvent()

	src := Source{}

	src.Set("MotionDetector", SourceVideo)

	e.SetSource(&src)

	for i := 0; i < 10; i++ {
		if i == 0 {
			s.Ref()
			s.Ref()
		}
		if e.GetInProgress() != false {
			t.Errorf("The event is has already been started!")
			fmt.Printf("%+v", e)
		}
		counter := 0
		for _ = range time.Tick(time.Millisecond * 100) {
			if counter >= 10 {
				break
			}

			s.Trigger(&src)
			counter++
			if e.GetInProgress() != true {
				t.Errorf("Trigger failed to start event.")
				fmt.Printf("%+v", e)
			}
		}

		time.Sleep(time.Second * 15)
		if e.GetInProgress() != false {
			t.Errorf("Event failed to stop after 10 seconds of inactivity")
		}

		e.Done()
		e.Done()
	}

	db.Close()
}
