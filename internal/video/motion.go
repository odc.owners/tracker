/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"image"
	"image/color"
	"time"
)

type MotionDetector struct {
	MinimumArea  float64
	DrawBoxes    bool
	DrawContours bool
	ShowWindow   bool
	state        *event.State
	Stream       *VideoStream

	/* internals */
	window          *gocv.Window
	src             event.Source
	lastTriggerSent time.Time
	imgDelta        gocv.Mat
	imgThresh       gocv.Mat
	mog2            gocv.BackgroundSubtractorMOG2
}

func NewMotionDetector() *MotionDetector {
	p := MotionDetector{}
	p.MinimumArea = 3000
	p.DrawContours = false
	p.DrawBoxes = false
	p.ShowWindow = false

	return &p
}

func (v *MotionDetector) Name() string {
	return "MotionDetector"
}

func (v *MotionDetector) SetState(state *event.State) {
	v.state = state
}

func (v *MotionDetector) SetMinimumArea(minimumArea float64) {
	v.MinimumArea = minimumArea
}

func (v *MotionDetector) EnableContours(enableCountours bool) {
	v.DrawContours = enableCountours
}

func (v *MotionDetector) EnableBoxing(enableBoxing bool) {
	v.DrawBoxes = enableBoxing
}

func (v *MotionDetector) EnableWindow(enableWindow bool) {
	v.ShowWindow = enableWindow
}

func (v *MotionDetector) Init(stream *VideoStream) {
	v.Stream = stream

	v.src = event.Source{}
	v.src.Set("MotionDetector", event.SourceVideo)

	if v.ShowWindow {
		v.window = gocv.NewWindow("Debug Window")
		//defer v.window.Close()
	}

	v.imgDelta = gocv.NewMat()
	//defer v.imgDelta.Close()

	v.imgThresh = gocv.NewMat()
	//defer v.imgThresh.Close()

	v.mog2 = gocv.NewBackgroundSubtractorMOG2()
	//defer v.mog2.Close()
}

func (v *MotionDetector) Close() {
	return
}

func (v *MotionDetector) Run(img gocv.Mat) {
	e := v.state.GetEvent()

	v.mog2.Apply(img, &v.imgDelta)

	gocv.Threshold(v.imgDelta, &v.imgThresh, 25, 255, gocv.ThresholdBinary)

	kernel := gocv.GetStructuringElement(gocv.MorphRect, image.Pt(3, 3))
	defer kernel.Close()

	dims := kernel.Size()

	step := kernel.Step()

	if kernel.Empty() || dims[0] <= 2 || dims[1] <= 2 || step < 1 {
		return
	}
	if v.imgThresh.Empty() || dims[0] <= 2 || dims[1] <= 2 || step < 1 {
		return
	}

	gocv.Dilate(v.imgThresh, &v.imgThresh, kernel)

	contours := gocv.FindContours(v.imgThresh, gocv.RetrievalExternal, gocv.ChainApproxSimple)
	for _, c := range contours {
		area := gocv.ContourArea(c)
		if area < v.MinimumArea {
			continue
		}

		if e.GetInProgress() == false {
			if e.GetIsComplete() == true {
				e.SetSource(&v.src)
				v.state.Trigger(&v.src)
				v.lastTriggerSent = time.Now()
			}
		} else {
			elapsed := time.Since(v.lastTriggerSent).Seconds()

			if elapsed >= 1 {
				v.state.Trigger(&v.src)
				v.lastTriggerSent = time.Now()
			}
		}

		if v.DrawBoxes == true {
			rect := gocv.BoundingRect(c)
			gocv.Rectangle(&img, rect, color.RGBA{0, 0, 255, 0}, 2)
		}
	}

	if v.ShowWindow == true {
		if !img.Empty() {
			v.window.IMShow(img)
			if v.window.WaitKey(1) == 27 {
				return
			}
		}
	}
}
