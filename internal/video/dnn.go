/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"time"
	"log"
	"image"
	"image/color"
)

type Dnn struct {
	MinimumArea  float64
	DrawBoxes    bool
	DrawContours bool
	ShowWindow   bool
	state        *event.State
	Stream       *VideoStream

	/* internals */
	window          *gocv.Window
	src             event.Source
	lastTriggerSent time.Time

	net				gocv.Net
	backend			gocv.NetBackendType
	target			gocv.NetTargetType
	model			string
	config			string
	ratio			float64
	mean			gocv.Scalar
	swapRGB			bool


}

func NewDnn() *Dnn {
	p := Dnn{}
	p.MinimumArea = 3000
	p.DrawContours = false
	p.DrawBoxes = false
	p.ShowWindow = false

	return &p
}

func (v *Dnn) Name() string {
	return "Dnn"
}

func (v *Dnn) SetState(state *event.State) {
	v.state = state
}

func (v *Dnn) SetMinimumArea(minimumArea float64) {
	v.MinimumArea = minimumArea
}

func (v *Dnn) EnableContours(enableCountours bool) {
	v.DrawContours = enableCountours
}

func (v *Dnn) EnableBoxing(enableBoxing bool) {
	v.DrawBoxes = enableBoxing
}

func (v *Dnn) EnableWindow(enableWindow bool) {
	v.ShowWindow = enableWindow
}

func (v *Dnn) Init(stream *VideoStream) {
	var (
		err error
	)

	v.Stream = stream
	log.Println("Initialized Dnn");

	if v.ShowWindow {
		v.window = gocv.NewWindow("Debug Window")
	}

	v.backend = gocv.ParseNetBackend("cuda")
	v.target = gocv.ParseNetTarget("cuda")

	v.model = "/skyhub/models/sd_mobilenet_v1_coco_2017_11_17.pb"
	v.config = "/skyhub/models/ssd_mobilenet_v1_coco_2017_11_17.pbtxt"

	v.net = gocv.ReadNet(v.model, v.config)

	if v.net.Empty() {
		log.Fatalf("Error reading network model from : %v %v\n", v.model, v.config)
	}

	//defer v.net.Close()

	err = v.net.SetPreferableBackend(gocv.NetBackendType(v.backend))

	if err != nil {
		log.Fatalf("SetPreferableBackend failed with '%s'\n", err)
	}

	err = v.net.SetPreferableTarget(gocv.NetTargetType(v.target))

	if err != nil {
		log.Fatalf("SetPreferableTarget failed with '%s'\n", err)
	}

	v.src = event.Source{}
	v.src.Set("Dnn", event.SourceVideo)

	v.ratio = 1.0 / 127.5
	//v.ratio = 1.0
	//v.mean = gocv.NewScalar(127.5, 127.5, 127.5, 0)
	v.mean = gocv.NewScalar(127.5, 127.5, 127.5, 0)
	v.swapRGB = true
}

func (v *Dnn) Close() {
	return
}

func (v *Dnn) Run(img gocv.Mat) {
	e := v.state.GetEvent()

	img.ConvertTo(&img, gocv.MatTypeCV32F)
	// convert image Mat to 300x300 blob that the object detector can analyze
	blob := gocv.BlobFromImage(img, v.ratio, image.Pt(300, 300), v.mean, v.swapRGB, false)

	// feed the blob into the detector
	v.net.SetInput(blob, "")

	// run a forward pass thru the network
	prob := v.net.Forward("")

	//performDetection(&img, prob)
	frame := &img
	for i := 0; i < prob.Total(); i += 7 {
		confidence := prob.GetFloatAt(0, i+2)
		if confidence > 0.5 {
			if e.GetInProgress() == false {
				if e.GetIsComplete() == true {
					e.SetSource(&v.src)
					v.state.Trigger(&v.src)
					v.lastTriggerSent = time.Now()
				}
			} else {
				elapsed := time.Since(v.lastTriggerSent).Seconds()

				if elapsed >= 1 {
					v.state.Trigger(&v.src)
					v.lastTriggerSent = time.Now()
				}
			}

			left := int(prob.GetFloatAt(0, i+3) * float32(frame.Cols()))
			top := int(prob.GetFloatAt(0, i+4) * float32(frame.Rows()))
			right := int(prob.GetFloatAt(0, i+5) * float32(frame.Cols()))
			bottom := int(prob.GetFloatAt(0, i+6) * float32(frame.Rows()))
			gocv.Rectangle(frame, image.Rect(left, top, right, bottom), color.RGBA{0, 255, 0, 0}, 2)
		}
	}

	if v.ShowWindow == true {
		if !img.Empty() {
			v.window.IMShow(img)
			if v.window.WaitKey(1) == 27 {
				return
			}
		}
	}

	prob.Close()
	blob.Close()
}

func performDetection(frame *gocv.Mat, results gocv.Mat) {
	for i := 0; i < results.Total(); i += 7 {
		confidence := results.GetFloatAt(0, i+2)
		if confidence > 0.5 {
			left := int(results.GetFloatAt(0, i+3) * float32(frame.Cols()))
			top := int(results.GetFloatAt(0, i+4) * float32(frame.Rows()))
			right := int(results.GetFloatAt(0, i+5) * float32(frame.Cols()))
			bottom := int(results.GetFloatAt(0, i+6) * float32(frame.Rows()))
			gocv.Rectangle(frame, image.Rect(left, top, right, bottom), color.RGBA{0, 255, 0, 0}, 2)
		}
	}
}
