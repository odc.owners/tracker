/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"fmt"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"log"
	"sync"
	"image"
)

type Record struct {
	Stream   *VideoStream
	DataPath string
	state    *event.State
	/* video recording state */
	videoUri     string
	thumbnailUri string
	videoWriter  *gocv.VideoWriter
	channels     []chan VideoRecording

	pipeline    string
	isRecording bool
	finish		sync.WaitGroup
}

type VideoRecording struct {
	EventId   string
	Uri       string
	Thumbnail string
}

func NewRecord() *Record {
	p := Record{}
	p.isRecording = false
	return &p
}

func (v *Record) Name() string {
	return "Record"
}

func (v *Record) SetState(state *event.State) {
	v.state = state
	v.state.Ref()
}

func (v *Record) SetDataPath(dataPath string) {
	v.DataPath = dataPath
}

func (v *Record) SetPipeline(pipeline string) {
	v.pipeline = pipeline
}

func (v *Record) Init(stream *VideoStream) {
	v.Stream = stream
}

func (v *Record) Close() {

	return
}

func (v *Record) Start(ev *event.Event) {
	var err error
	v.isRecording = true

	v.videoUri = fmt.Sprintf(v.pipeline, v.DataPath, ev.GetId())

	v.videoWriter, err = gocv.VideoWriterFile(
		v.videoUri,
		"0",
		float64(v.Stream.framePerSecond),
		v.Stream.frameWidth,
		v.Stream.frameHeight,
		true)

	if err != nil {
		log.Printf("Video: Failed to create writer: %s\n", err)
	}
}

func (v *Record) generateThumbnail(ev *event.Event, img *gocv.Mat) (success bool) {
	defer v.finish.Done()
	
	var resizeMat gocv.Mat

	resizeMat = gocv.NewMat()

	v.thumbnailUri = fmt.Sprintf("%s/thumbnail/%s.webp", v.DataPath, ev.GetId())

	width := 300
	height := 200
	
	if img.Empty() { 
		log.Printf("Thumbnail for event %s is empty, ignoring\n", ev.GetId())
		return
	}

	gocv.Resize(*img, &resizeMat, image.Pt(width, height), 0, 0, gocv.InterpolationArea)
	options := []int{gocv.IMWriteWebpQuality, 60}
	success = gocv.IMWriteWithParams(v.thumbnailUri, resizeMat, options)

	if success == false {
		log.Printf("Failed writing thumbnail for event '%s'\n", ev.GetId())
	}

	resizeMat.Close()
	return
}

func (v *Record) Stop(ev *event.Event) {
	var (
		videoRecording VideoRecording
	)
	v.isRecording = false

	// make sure everything is done before we close writer
	// specifically waiting for thumbnail generation
	v.finish.Wait()
	v.videoWriter.Close()

	// notify callers of new video recording
	videoRecording.EventId = ev.GetId()
	videoRecording.Uri = v.videoUri
	videoRecording.Thumbnail = v.thumbnailUri

	for _, ch := range v.channels {
		ch <- videoRecording
	}

	v.videoWriter = nil

	v.videoUri = ""
	v.thumbnailUri = ""
}

func (v *Record) Register(ch chan VideoRecording) {
	v.channels = append(v.channels, ch)
}

func (v *Record) GetIsRecording() bool {
	return v.isRecording
}

func (v *Record) Write(img *gocv.Mat) {
	var (
		ev *event.Event
	)

	ev = v.state.GetEvent()

	if ev.GetInProgress() {
		if !v.GetIsRecording() {
			v.Start(ev)
			// increment the wait group so the thumbnail
			// always gets generated before we stop recording
			v.finish.Add(1)
			go v.generateThumbnail(ev, img)
		}

		err := v.videoWriter.Write(*img)

		if err != nil {
			log.Printf("Video: Recording write error on event '%s': %s\n", ev.GetId(), err)
		}
	} else {
		if ev.GetIsComplete() == false {
			if v.GetIsRecording() {
				v.Stop(ev)
				ev.Done()
			}
		}
	}
}
